package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;

import models.MEmployees;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Employees.*;

import java.util.Map;

public class CEmployees extends Controller {




    public static Result RenderAddEmployee() {

        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(AddEmployees.render("Add Employees"));
    }
    public static Result RenderEditEmployee(Long id){
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else {
            System.out.println("the aid is " + session("aid"));

            MEmployees mEmployees = MEmployees.findEmployees.byId(id);
            return ok(EditEmployees.render("Edit Employee", mEmployees));
        }
        }



    public static Result RenderViewEmployee() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(ViewEmployees.render("View Employees"));
    }


    public static Result RenderImport() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));

        return ok(importCsv.render("import csv"));
    }



    public static Result EditEmployee(Long id){
        MEmployees mEmployees =new MEmployees();
        mEmployees.aid=id;
        mEmployees.FullName=Form.form().bindFromRequest().get("FullName");
        mEmployees.Email=Form.form().bindFromRequest().get("Email");
        mEmployees.IdNumber=Form.form().bindFromRequest().get("IdNumber");
        mEmployees.Department=Form.form().bindFromRequest().get("Department");
        mEmployees.EmploymentNumber=Form.form().bindFromRequest().get("EmploymentNumber");
        Ebean.update(mEmployees);
        return redirect(routes.CEmployees.RenderViewEmployee());
    }



    public static Result activateEmployee(Long id){
        MEmployees mEmployees = new MEmployees();
        mEmployees.aid=id;
        mEmployees.isActive="1";
        mEmployees.update();

        return redirect(routes.CEmployees.RenderViewEmployee());
    }
    public static Result deactivateEmployee(Long id){
        MEmployees mEmployees = new MEmployees();
        mEmployees.aid=id;
        mEmployees.isActive="0";
        mEmployees.update();

        return redirect(routes.CEmployees.RenderViewEmployee());
    }

    public static Result ReceiveEmployees() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String IdNumber = requestform.get("IdNumber");
        String Department = requestform.get("Department");
        String EmploymentNumber = requestform.get("EmploymentNumber");

        System.out.println("FullName: "+FullName);
        System.out.println("Email: "+Email);
        System.out.println("IdNumber: "+IdNumber);
        System.out.println("Department: "+Department);
        System.out.println("EmploymentNumber: "+EmploymentNumber);

        return ok();
    }

        public static Result AddEmployees () {
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String IdNumber = requestform.get("IdNumber");
        String Department = requestform.get("Department");
        String EmploymentNumber = requestform.get("EmploymentNumber");
        String PhoneNumber = requestform.get("PhoneNumber");

      int len=PhoneNumber.length();

        if((MEmployees.findEmployee(IdNumber)==null) && (MEmployees.findEmployeeNumber(EmploymentNumber)==null) && (len>=10 && len<=15) )

        {
            System.out.println("Matched");
            MEmployees mEmployees = new MEmployees();
            mEmployees.FullName = FullName;
            mEmployees.Email = Email;
            mEmployees.IdNumber = IdNumber;
            mEmployees.Department = Department;
            mEmployees.EmploymentNumber = EmploymentNumber;
            mEmployees.PhoneNumber = PhoneNumber;
            mEmployees.isActive = "1";
            mEmployees.save();
            return redirect(routes.CEmployees.RenderViewEmployee());
        }
        else
            flash("typeerror", "employee with such Id or employment number exists!!!!");
            return redirect(routes.CEmployees.RenderAddEmployee());

            }

        public static Result EmployeeList () {
            Map<String, String[]> params = request().queryString();

            Integer iTotalRecords = MEmployees.findEmployees.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

            /**
             * Get sorting order and column
             */
            String sortBy = "Email";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "Email";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "EmploymentNumber";
                    break;
            }

            /**
             * Get page to show from database
             * It is important to set setFetchAhead to false, since it doesn't benefit a stateless application at all.
             */
            Page<MEmployees> areaPage = MEmployees.findEmployees.where(
                    Expr.or(
                            Expr.ilike("Email", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("EmploymentNumber", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();

            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MEmployees cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                //    System.out.println("in data table fetch: " + cc.UserName);

                row.put("aid", cc.aid);
                row.put("Email", cc.Email);
                row.put("FullName", cc.FullName);
                row.put("isActive", cc.isActive);
                row.put("IdNumber", cc.IdNumber);
                row.put("Department", cc.Department);
                row.put("EmploymentNumber", cc.EmploymentNumber);
                row.put("isActive", cc.isActive);
                anc.add(row);
            }

            return ok(result);

        }
   /* public static void upload(String qqfile) {


        if (request.isNew) {

            FileOutputStream moveTo = null;

            Logger.info("Name of the file %s", qqfile);
            // Another way I used to grab the name of the file
            String filename = request.headers.get("x-file-name").value();

            Logger.info("Absolute on where to send %s", Play.getFile("").getAbsolutePath() + File.separator + "uploads" + File.separator);
            try {

                InputStream data = request.body;


                moveTo = new FileOutputStream(new File(Play.getFile("").getAbsolutePath()) + File.separator + "uploads" + File.separator + filename);
                IOUtils.copy(data, moveTo);

            } catch (Exception ex) {

                // catch file exception
                // catch IO Exception later on
                renderJSON("{success: false}");
            }

        }


        renderJSON("{success: true}");
    }
*/
}
