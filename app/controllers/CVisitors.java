package controllers;


import models.MCards;
import models.MUsers;
import models.MVisitorRecords;
import models.MVisitors;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Visitors.*;

import java.util.Date;
import java.util.Map;
public class CVisitors extends Controller {

    public static Result RenderViewVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewVisitors.render("Visitors")); }

    public static Result RenderViewReccurrentVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewRecurringVisitors.render("Recurring Visitors"));
    }

    public static Result RenderViewImage(Long id){
        MVisitorRecords mVisitorRecords=MVisitorRecords.findVisitorsRecords.byId(id);
        return ok(DisplayImage.render("mvisitor records",mVisitorRecords));

    }



    public static Result RenderViewCurrentVisitors() {
        MUsers user = CUsers.isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewCurrentVisitors.render("View Current Visitors"));
    }

    public static Result ReceiveVisitors(){
        DynamicForm requestform = Form.form().bindFromRequest();
        String FullName = requestform.get("FullName");
        String Email = requestform.get("Email");
        String Country = requestform.get("Country");
        String Id = requestform.get("Id");
        String Host = requestform.get("Host");
        String Company = requestform.get("Company");
        String Type = requestform.get("VisitType");
        String Gender = requestform.get("Gender");
        String Languages = requestform.get("Languages");
        String TimeIn = requestform.get("TimeIn");
        String TimeOut = requestform.get("TimeOut");
        String Date = requestform.get("Date");
        String Image = requestform.get("Image");

        System.out.println("FullName: "+FullName);
        System.out.println("Email: "+Email);
        System.out.println("Country: "+Country);
        System.out.println("Id: "+Id);
        System.out.println("Host: "+Host);
        System.out.println("Company: "+Company);
        System.out.println("VisitType: "+Type);
        System.out.println("Gender: "+Gender);
        System.out.println("Languages: "+Languages);
        System.out.println("TimeIn: "+TimeIn);
        System.out.println("TimeOut: "+TimeOut);
        System.out.println("Date: "+Date);
        System.out.println("Image: "+Image);
        return redirect(routes.CVisitors.RenderViewVisitors());
    }
    public static Result ActivateVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        mVisitors.aid=id;
        mVisitors.isActive="1";
        mVisitors.update();

        return redirect(routes.CVisitors.RenderViewVisitors());
    }
    public static Result DeleteVisitor(Long id){
        MVisitors mVisitors = new MVisitors();
        long Aid=MVisitors.findVisitorById(id.toString()).aid;
        mVisitors.isDeleted="1";
        mVisitors.update(Aid);

        //getting card number and free the cards too
        String cardnumber=MVisitors.findVisitorById(id.toString()).cardNumber;
        MCards mCards=new MCards();
        long Aid2=MCards.exitCard(cardnumber).aid;
        mCards.isActive="0";
        mCards.update(Aid2);

        return redirect(routes.CVisitors.RenderViewVisitors());
    }

    public static Result VisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("isDeleted",  "0" ),

                        Expr.or(
                        Expr.ilike("FullName", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("aid", "%" + filter + "%"),
                                Expr.ilike("NationalId", "%" + filter + "%")
                        )
                )
                )
                )
                        .orderBy(sortBy + " " + order + ", aid " + order)
                        .findPagingList(pageSize).setFetchAhead(false)
                        .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            //row.put("Image",cc.Image);
            anc.add(row);
        }
        return ok(result);
    }


    public static Result ReccuringVisitorsList(){
        Map<String, String[]> params = request().queryString();

        Integer iTotalRecords = MVisitorRecords.findVisitorsRecords.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


        String sortBy = "NoOfTimes";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0]))
        {
            case 0:
                sortBy = "NoOfTimes";
                break;
            case 1:
                sortBy = "FullName";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitorRecords> areaPage = MVisitorRecords.findVisitorsRecords.where(
                Expr.or(
                        Expr.ilike("NoOfTimes", "%" + filter + "%"),
                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.ilike("NationalId", "%" + filter + "%")
                        )
                )

        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitorRecords cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);
            row.put("Country", cc.Country);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Image",cc.Image);


            row.put("NoOfTimes",cc.NoOfTimes);
            anc.add(row);
        }
        return ok(result);
    }






    public static Result CurrentVisitorsList(){
        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;

        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }
        /**
         * Get page to show from database
         * It is important to set setFetchAhead to false, since it doesn't benefit a stateless application at all.
         */
        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("isActive",  "1" ),
                        Expr.and(Expr.eq("isDeleted",  "0" ),
                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.ilike("aid", "%" + filter + "%")
                               // Expr.ilike("Id", "%" + filter + "%")
                        )
                )
                )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();

            //    System.out.println("in data table fetch: " + cc.);
            row.put("FullName", cc.FullName);
            row.put("aid", cc.aid);
            row.put("NationalId", cc.NationalId);
            row.put("Email", cc.Email);
            row.put("Company", cc.Company);

            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            row.put("cardNumber",cc.cardNumber);
            //row.put("Image",cc.Image);
            anc.add(row);
        }
        return ok(result);
    }

    public static Result ExitedVisitors(){
        Map<String, String[]> params = request().queryString();
        Integer iTotalRecords = MVisitors.findVisitors.findRowCount();
        String filter = params.get("sSearch")[0];
        Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
        Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;





        String sortBy = "FullName";
        String order = params.get("sSortDir_0")[0];

        switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
            case 0:
                sortBy = "FullName";
                break;
            case 1:
                sortBy = "aid";
                break;
            case 2:
                sortBy = "NationalId";
                break;
        }

        Page<MVisitors> areaPage = MVisitors.findVisitors.where(
                Expr.and(Expr.eq("isActive",  "0" ),
                        Expr.and(Expr.eq("isDeleted",  "0" ),

                        Expr.or(
                                Expr.ilike("FullName", "%" + filter + "%"),
                                Expr.ilike("aid", "%" + filter + "%")
                                // Expr.ilike("Id", "%" + filter + "%")
                        )
                )
                )
        )
                .orderBy(sortBy + " " + order + ", aid " + order)
                .findPagingList(pageSize).setFetchAhead(false)
                .getPage(page);
        Integer iTotalDisplayRecords = areaPage.getTotalRowCount();
        ObjectNode result = Json.newObject();

        result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
        result.put("iTotalRecords", iTotalRecords);
        result.put("iTotalDisplayRecords", iTotalDisplayRecords);

        ArrayNode anc = result.putArray("aaData");
        for (MVisitors cc : areaPage.getList() ) {
            ObjectNode row = Json.newObject();
            row.put("FullName", cc.FullName);
            row.put("NationalId", cc.NationalId);
            row.put("Company", cc.Company);
            row.put("aid", cc.aid);
            row.put("TimeOut",cc.TimeOut);
            row.put("TimeIn", cc.TimeIn);
            row.put("Country", cc.Country);
            row.put("Type", cc.Type);
            row.put("Host", cc.Host);
            row.put("Language", cc.Languages);
            row.put("Gender",cc.Gender);
            row.put("Date",cc.Date);
            row.put("cardNumber",cc.cardNumber);
            anc.add(row);
        }
        return ok(result);
    }



}








