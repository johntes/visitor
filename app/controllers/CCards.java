package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.*;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Cards.*;
import views.html.Users.*;


import java.security.GeneralSecurityException;
import java.util.Map;

import static controllers.CUsers.isLoggedIn;
import static controllers.EncryptionTest.encrypt;

public class CCards extends Controller{

    public static Result RenderAddCard() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(AddCards.render("Add cards"));
    }


    public static Result RenderViewCard() {
        MUsers user = isLoggedIn();
        if (user == null) {
            return redirect(routes.CLogin.RenderLoginInterface());
        }
        else
            System.out.println("the aid is "+session("aid"));
        return ok(ViewCards.render("View Card"));
    }


    public static Result ActivateCard(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="1";
        mUsers.update();
        return redirect(routes.CCards.RenderViewCard());
    }

    public static Result DeActivateCard(Long id){
        MCards mUsers = new MCards();
        mUsers.aid=id;
        mUsers.admin_activated="0";
        mUsers.update();

        return redirect(routes.CCards.RenderViewCard());
    }


    public static Result ReceiveCard() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        System.out.println("UserName: "+cardNumber);
        return ok();
    }


    public static Result AddCard() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String cardNumber = requestform.get("cardNumber");
        MCards mCards = new MCards();
        mCards.cardNumber = cardNumber;
        mCards.isActive = "0";
        mCards.admin_activated = "0";
        mCards.save();
        return redirect(routes.CCards.RenderViewCard());

        }



        public static Result cardList () {
            Map<String, String[]> params = request().queryString();
            Integer iTotalRecords = MCards.findCards.findRowCount();
            String filter = params.get("sSearch")[0];
            Integer pageSize = Integer.valueOf(params.get("iDisplayLength")[0]);
            Integer page = Integer.valueOf(params.get("iDisplayStart")[0]) / pageSize;


            String sortBy = "cardNumber";
            String order = params.get("sSortDir_0")[0];

            switch (Integer.valueOf(params.get("iSortCol_0")[0])) {
                case 0:
                    sortBy = "cardNumber";
                    break;
                case 1:
                    sortBy = "aid";
                    break;
                case 2:
                    sortBy = "admin_activated";
                    break;
            }

            Page<MCards> areaPage = MCards.findCards.where(
                    Expr.or(
                            Expr.ilike("cardNumber", "%" + filter + "%"),
                            Expr.or(
                                    Expr.ilike("aid", "%" + filter + "%"),
                                    Expr.ilike("isActive", "%" + filter + "%")
                            )
                    )
            )
                    .orderBy(sortBy + " " + order + ", aid " + order)
                    .findPagingList(pageSize).setFetchAhead(false)
                    .getPage(page);

            Integer iTotalDisplayRecords = areaPage.getTotalRowCount();


            ObjectNode result = Json.newObject();
            result.put("sEcho", Integer.valueOf(params.get("sEcho")[0]));
            result.put("iTotalRecords", iTotalRecords);
            result.put("iTotalDisplayRecords", iTotalDisplayRecords);

            ArrayNode anc = result.putArray("aaData");

            for (MCards cc : areaPage.getList()) {
                ObjectNode row = Json.newObject();
                row.put("aid", cc.aid);
                row.put("cardNumber", cc.cardNumber);
                row.put("admin_activated", cc.admin_activated);
                String assigned=cc.isActive;
                if(assigned.equals("1"))
                {
                    row.put("assigned","yes");
                }
                else
                {
                    row.put("assigned","no");
                }


                anc.add(row);
            }

            return ok(result);

        }



    }
