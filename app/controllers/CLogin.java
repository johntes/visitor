package controllers;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import models.MEmployees;
import models.MUsers;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;

import scala.Array;
import views.html.Login.*;


public class CLogin extends Controller {

    public static Result RenderLoginInterface() {
        return ok(LoginInterface.render("LoginInterface"));
    }

    public static Result authenticateLogin() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String UserName = requestform.get("UserName");
        String Password = requestform.get("Password");


        if (MUsers.authenticateWebUser(UserName, Password) != null)  {
            session().clear();
            setSessions(MUsers.authenticateWebUser(UserName,Password));
            System.out.println("session set");

            return redirect(routes.CDashboard.dashboardRender());

        }

        else if(MEmployees.authenticateEmployee(UserName,Password)!=null){
            session().clear();
            setSessionsEmployee(MEmployees.authenticateEmployee(UserName,Password));
            System.out.println("The employee session has ben set");
            return redirect(routes.CExpectedVisitors.RenderAddExpectedVisitors());
        }


        else {
            flash("typeerror", "wrong UserName or Password");
            return redirect(routes.CLogin.RenderLoginInterface());
        }
    }

    public static Result logOut(){
        session().clear();
        return redirect(routes.CLogin.RenderLoginInterface());


    }

    private static void setSessions(MUsers userMst)
    {
        session().clear();
        session("aid",String.valueOf(userMst.aid));
    }

    private static void setSessionsEmployee(MEmployees emp)
    {
        session().clear();
        session("aid",String.valueOf(emp.aid));
    }


}