package controllers;

import models.*;
import play.*;
import play.mvc.*;
import views.html.Cards.ViewCards;
import views.html.Dashboard.*;

import models.MUsers;
import models.MVisitorRecords;
import models.MVisitors;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Result;
import views.html.Visitors.*;

import java.util.Date;
import java.util.Map;

public class CDashboard extends Controller {

    public static  Result renderChart(){
        return ok(chat.render("render charts"));
    }



    public static Result dashboardRender() {
        int numberOfAdmins = MUsers.findUsers.findRowCount();
        int numberOfEmployees = MEmployees.findEmployees.findRowCount();
        int numberOfTotalVisitors = MVisitors.findVisitors.findRowCount();
        int numberOfCurrentVisitors = MVisitors.findActiveVisitors().size();
        int numberOfExitedVisitors = MVisitors.findDiActivetedVisitors().size();



        return ok(dashboard.render("DashBoard",numberOfAdmins,numberOfEmployees,numberOfCurrentVisitors,numberOfTotalVisitors,numberOfExitedVisitors));
    }



   /* public  static Result employeeDashboardRender(){
        MEmployees emp=isLoggedIn();

        if(isLoggedIn()==null){
            return redirect(routes.CLogin.RenderLoginInterface());
        }

        else


       // int numberOfExpectedVisitors=MExpectedVisitors.findRecurrentVistorList()

    }*/


    public static Result return_visitors(){
        return ok(Json.toJson(MVisitors.findAll()));
    }


    public static MEmployees isLoggedIn() {
        String aid = session("aid");
        try {
            MEmployees user = MEmployees.findEmployees.byId(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }
    }

}
