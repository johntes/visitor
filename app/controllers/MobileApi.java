package controllers;
import models.*;
import play.*;
import play.mvc.*;
import play.mvc.Result;
import play.libs.Json;
import play.mvc.Controller;
//import org.Codehaus.jackson.node.ObjectNode;
import  com.avaje.ebean.Ebean;
import  com.avaje.ebean.Expr;
import  com.avaje.ebean.Page;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import play.Logger;
//import play.DynamicForm;
import play.data.Form;

import play.data.DynamicForm;

import java.util.ArrayList;
import java.util.List;
//import com.innovatrics.mrz.types.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//import static controllers.CUsers.isLoggedIn;

public class MobileApi extends Controller {

    private static void setSessions(MUsers userMst)
    {
        session().clear();
        session("aid",String.valueOf(userMst.aid));

        /*MrzRecord ex = MrzParser.parse(m);
        JOptionPane.showMessageDialog(frame, "Parse successfull: " + ex);
        String mpe1 = ex.sex.toString();
        String r1 = ex.givenNames.toString() + " " + ex.surname.toString();
        String IdNumber = ex.documentNumber.toString();
        String country = ex.issuingCountry.toString();
        System.out.println("the name is :" + r1);
        System.out.println("the IdNumber is :" + IdNumber);
        System.out.println("the country is :" + country);
        System.out.println("the sex is :" + mpe1);*/
    }



    public static MUsers isLoggedIn() {
        String aid = session("aid");
        try {
            MUsers user = MUsers.findUsers.byId(Long.parseLong(aid));
            return user;
        }catch(Exception e){
            return null;
        }

    }


public  static Result logOutMobile(){
    ObjectNode objectNode;
    objectNode = Json.newObject();
    MUsers user = isLoggedIn();
        if(user!=null){
            session().clear();
            objectNode.put("responseCode","200");
            return  ok(objectNode);
        }
        else
            objectNode.put("responseCode","not logged in currently");
        return  ok(objectNode);
}



    public static Result authenticateMobileUsers() {
        System.out.println("Reaching the server");
        DynamicForm requestform = Form.form().bindFromRequest();
        String Usernem = requestform.get("username");
        String Password = requestform.get("pass");

        System.out.println("username: "+Usernem);
        System.out.println("Password: "+Password);

        ObjectNode objectNode;
        objectNode = Json.newObject();

        MUsers mUsers=MUsers.authenticateMobileUser(Usernem, Password);

        if (mUsers != null) {
            session().clear();
            setSessions(MUsers.authenticateMobileUser(Usernem,Password));
            System.out.println("session set");
            //ession

            objectNode.put("responseCode", "200");

            return ok(objectNode);

        } else {
            objectNode.put("responseCode", "201");
            return ok(objectNode);
        }

    }

    public static Result ReceiveProfileUpdate() {
        DynamicForm requestform = Form.form().bindFromRequest();
        String Usernem = requestform.get("USERNAME");
        System.out.println(Usernem);
        String Password = requestform.get("PASSWORD");
        System.out.println(Password);
        ObjectNode objectNode;
        objectNode = Json.newObject();
        String IdNumber=MUsers.authenticateEditProfile(Usernem, Password).EmploymentNumber;
        MUsers mUsers=MUsers.findUserById(IdNumber);
        mUsers.UserName=Usernem;
        mUsers.Password=Password;
        mUsers.update();
        objectNode.put("responseCode", "200");
        return ok(objectNode);
    }


    public static Result ReceiveVisitorsDetails(){

            System.out.println("HERE.....");
            ObjectNode objectNode;
            objectNode = Json.newObject();
            MVisitors mVisitors = new MVisitors();
            MVisitorRecords mVisitorRecords=new MVisitorRecords();
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DynamicForm requestform = Form.form().bindFromRequest();
            String FullName = requestform.get("Fullname");
            System.out.println("Fullname:  " + FullName);
            String Email = requestform.get("Email");
            System.out.println("Email: " + Email);
            String SelectedCountry = requestform.get("SelectedCountry");
            System.out.println("SelectedCountry: " + SelectedCountry);
            String Id = requestform.get("Id");
            String Company = requestform.get("Company");
            String Visittype = requestform.get("VisitType");
            String Host = requestform.get("Host");
            String TimeIn = requestform.get("TimeIn");
            System.out.println("TimeIn:" + TimeIn);
            String Languages = requestform.get("Language");
            String TimeOut = requestform.get("TimeOut");
            String Gender = requestform.get("Gender");
            String Image = requestform.get("base64image");
            System.out.println("Image:" + Image);
            String Date = requestform.get("Date");

            String generated_by=requestform.get("generated_by");
            String QrImage=requestform.get("QrImage");


            //String cardNumber = requestform.get("cardNumber");
             //System.out.println("card number:" + cardNumber);
       /* if(MCards.findUnavailableCard(cardNumber)!=null)
        {
            objectNode.put("responseCode","202");
            return ok(objectNode);
        }

        else if(MCards.findAvailableCard(cardNumber)==null){
            objectNode.put("responseCode","201");
            return ok(objectNode);
        }*/

        //check if the visitor is already in .if available return response code 203
        if(MVisitors.checkVisitorStatus(Id)!=null){
            objectNode.put("responseCode","203");
            return ok(objectNode);
        }

        else
            if(MVisitorRecords.findVisitorById(Id)!=null) {


                System.out.println("id is:" + Id);
                long aid2 = MVisitorRecords.findVisitorById(Id).aid;
                System.out.println("aid is:" + aid2);
                int numOfVisits = MVisitorRecords.findVisitorById(Id).NoOfTimes;
                System.out.println("aid is:" + numOfVisits);
                //UPDATING TABLE VISITOR RECORD TO INCREASE NUMBER OF ATTENDANCE TIMES AND CREATING A NEW RECORD
                mVisitorRecords.NoOfTimes = numOfVisits+1;
                mVisitorRecords.update(aid2);
            }

                //if(MVisitorRecords.findVisitorById(Id)==null)
        else
                {
                    MVisitorRecords mVisitorRecords1=new MVisitorRecords();
                    mVisitorRecords1.FullName = FullName;
                    mVisitorRecords1.Email = Email;
                    mVisitorRecords1.Country = SelectedCountry;
                    mVisitorRecords1.NationalId = Id;
                    mVisitorRecords1.Company = Company;
                    mVisitorRecords1.Languages = Languages;
                    mVisitorRecords1.Gender = Gender;
                    mVisitorRecords1.Image = Image;
                    mVisitorRecords1.NoOfTimes=1;
                    mVisitorRecords1.save();

                }
                mVisitors.FullName = FullName;
                mVisitors.Email = Email;
                mVisitors.Country = SelectedCountry;
                mVisitors.NationalId = Id;
                mVisitors.Company = Company;
                mVisitors.TimeIn = TimeIn;
                mVisitors.Host = Host;
                mVisitors.Languages = Languages;
                mVisitors.TimeOut = "";
                mVisitors.Type = Visittype;
                mVisitors.Gender = Gender;
                mVisitors.Date = Date;
                mVisitors.Image = Image;
                mVisitors.isActive = "1";
               // mVisitors.cardNumber = cardNumber;
                mVisitors.QrImage=QrImage;
                mVisitors.isDeleted="0";
                mVisitors.save();


                MQR mqr=new MQR();
        mqr.isActive="1";
        mqr.time_generated=TimeIn;
        mqr.generated_by=generated_by;
        mqr.VisitorId=Id;
        mqr.QrImage=QrImage;
        mqr.save();

                /*MCards mCards=new MCards();
                long Aid=MCards.exitCard(cardNumber).aid;
                mCards.isActive="1";
                mCards.update(Aid);*/

                //mVisitorRecords.save();
                objectNode.put("responseCode", "200");
                return ok(objectNode);
            }




    public static Result ReceiveExitingVisitors(){

        System.out.println("HERE.....");
        ObjectNode objectNode;
        objectNode = Json.newObject();
        MUsers user = isLoggedIn();
        System.out.println("HERE.....");
            DynamicForm requestform = Form.form().bindFromRequest();
            String VisitorId = requestform.get("VisitorId");
            String TimeOut = requestform.get("TimeOut");
            System.out.println("cardNumber:" + VisitorId);
            MVisitors mVisitors = new MVisitors();
           //MCards mCards=new MCards();


            if (MVisitors.checkVisitor(VisitorId, "1") != null) {
                long id = MVisitors.checkVisitor(VisitorId, "1").aid;
                mVisitors.isActive = "0";
                mVisitors.TimeOut = TimeOut;
                mVisitors.update(id);
                MQR mqr=new MQR();

                if(MQR.exitQr(VisitorId)!=null){
                    long Aid=MQR.exitQr(VisitorId).aid;
                    mqr.isActive="0";
                    mqr.update(Aid);
                    objectNode.put("responseCode", "200");

                }

                /*if(MCards.exitCard(VisitorId)!=null){
                    long Aid=MCards.exitCard(cardNumber).aid;
                    mCards.isActive="0";
                    mCards.update(Aid);
                    objectNode.put("responseCode", "200");
                }*/

            } else {
                objectNode.put("responseCode", "201");
            }
            return ok(objectNode);
    }


//GET ALREADY EXISTING VISITOR DETAILS
    public static Result ReturnVisitorsDetails(){
        System.out.println("Getting visitor details");
        ObjectNode result;
        DynamicForm form=Form.form().bindFromRequest();
        String ID=form.get("IdNumber");
        result = Json.newObject();

        if(MVisitorRecords.findVisitorById(ID)!=null){

            String FullName = MVisitorRecords.findVisitorById(ID).FullName;
            String Email = MVisitorRecords.findVisitorById(ID).Email;
            String SelectedCountry = MVisitorRecords.findVisitorById(ID).Country;
            String Id = MVisitorRecords.findVisitorById(ID).NationalId;
            String Company = MVisitorRecords.findVisitorById(ID).Company;
            String Languages = MVisitorRecords.findVisitorById(ID).Languages;
            String Gender = MVisitorRecords.findVisitorById(ID).Gender;
            String Image = MVisitorRecords.findVisitorById(ID).Image;

            result.put("responseCode","200");
            result.put("FullName",FullName);
            result.put("Email",Email);
            result.put("Country",SelectedCountry);
            result.put("NationalId",Id);
            result.put("Company",Company);
            result.put("Gender",Gender);
            result.put("Languages",Languages);
            result.put("Image",Image);

           //result.put("visitorList",Json.toJson(MVisitorRecords.findRecurrentVistorList(ID)));
            return  ok(result);
            //return ok(Json.toJson(MVisitorRecords.findRecurrentVistorList(ID)));
        }
        else
        result.put("responseCode","201");
        return ok(result);
    }



    public static Result return_users() {
        System.out.println("Reaching on the server");
        return ok(Json.toJson(MUsers.findAll()));
    }



    //return current visitots in the company
    public static Result return_Visitor() {
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching on the server");
        System.out.println("HERE.....");
            int current = MVisitors.findActiveVisitors().size();
            int total = MVisitors.findAll().size();
            int exited = MVisitors.findDiActivetedVisitors().size();
            System.out.println("the size of currrent visitors is: " + current);
            System.out.println("the size total visitors is: " + total);
            System.out.println("the size exited visitors is: " + exited);
            objectNode.put("current", current);
            objectNode.put("total", total);
            objectNode.put("exited", exited);
            return ok(objectNode);
    }



    //RETURN EXITED VISITORS
    public static Result return_exitedVisitor() {
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching on the server");
        int sized=MVisitors.findDiActivetedVisitors().size();
        System.out.println("the size of all active visitors is: "+sized);
        objectNode.put("responseCode", sized);
        return ok(objectNode);
    }

    //return total visitots in the company
    public static Result return_totalVisitors(){
        ObjectNode objectNode;
        objectNode = Json.newObject();
        System.out.println("Reaching  on the server");
        int sized=MVisitors.findAll().size();
        System.out.println("the size of all the visitors is: "+sized);
        objectNode.put("responseCode", sized);
        return ok(objectNode);
    }

//RETURN EMPLOYEE LIST
    public static Result return_employees(){
        return ok(Json.toJson(MEmployees.findAll()));
    }



    public static Result check_card(){
        DynamicForm form=Form.form().bindFromRequest();
        String card_number=form.get("card_number");
        ObjectNode objectNode=Json.newObject();

        if(MCards.findUnavailableCard(card_number)!=null)
        {
            objectNode.put("responseCode","card is in use");
            return ok(objectNode);
        }
       else if(MCards.findAvailableCard(card_number)==null){
            objectNode.put("responseCode","no such card");
            return ok(objectNode);
        }

        else
            objectNode.put("responseCode","200");
        return ok(objectNode);

    }

    //Receive generated qr codes
    public static Result receive_qr(){
        ObjectNode objectNode;
        objectNode=Json.newObject();
        DynamicForm form=Form.form().bindFromRequest();
        String id=form.get("VisitorId");
        String time=form.get("time_generated");
        String generated_by=form.get("generated_by");
        String image=form.get("QrImage");

        System.out.println("the Id is : "+id);
        System.out.println("the image is : "+image);
        System.out.println("generated by: "+generated_by);
        System.out.println("generated at: "+time);

        MQR mqr=new MQR();
        mqr.VisitorId=id;
        mqr.generated_by=generated_by;
        mqr.QrImage=image;
        mqr.time_generated=time;
        mqr.isActive="1";
         mqr.save();

        objectNode.put("responseCode","200");
        return ok(objectNode);


    }

}

