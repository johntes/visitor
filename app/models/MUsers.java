package models;


import java.util.List;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Constraint;
import java.util.List;
import java.util.Map;
import java.util.*;

@Entity
public class MUsers extends Model{
    @Id public long aid;

    @Constraints.Required
    public String UserName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String Password;

    @Constraints.Required
    public String UserType;

    @Constraints.Required
    public String ConfirmPassword;

    @Constraints.Required
    public String EmploymentNumber;

    @Constraints.Required
    public String isActive;



    public static Model.Finder<Long, MUsers> findUsers = new Model.Finder<Long, MUsers>(Long.class,  MUsers.class);

    public static List<MUsers> findAll() { return findUsers.where().findList();
    }
    public static List<MUsers> findActiveUsers() { return findUsers.where().eq("isActive", "1").findList();
    }

    public static Map<String, String> User() {
        LinkedHashMap<String,String> options=new LinkedHashMap<>();
        for(MUsers c: MUsers.findUsers.
                where().eq("isActive","1")
                .orderBy("UserName").findList()){
            options.put(Long.toString(c.aid),c.UserName );
        }
        return options;
    }

    public static MUsers authenticateMobileUser(String id, String pwd){
        return MUsers.findUsers.where().eq("UserName",id).
                eq("Password",pwd).eq("UserType","mobile").eq("isActive","1")
                .findUnique();
    }


    public static MUsers authenticateWebUser(String id, String pwd){
        return MUsers.findUsers.where().eq("UserName",id).
                eq("Password",pwd).eq("UserType","web").eq("isActive","1")
                .findUnique();
    }

    public static MUsers findUserById(String EmploymentNumber){
        return MUsers.findUsers.where().eq("EmploymentNumber",EmploymentNumber)
                .findUnique();
    }

    public static MUsers authenticateEditProfile(String id,String pwd){
        return MUsers.findUsers.where().eq("UserName",id).
                eq("Password",pwd)
                .findUnique();
    }



}
