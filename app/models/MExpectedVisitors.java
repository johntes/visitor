package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class MExpectedVisitors extends Model{
    @Id
    public long aid;

    @Constraints.Required
    public String FullName;

    @Constraints.Required
    public String Email;

    @Constraints.Required
    public String Country;

    @Constraints.Required
    public String NationalId;

    @Constraints.Required
    public String Address;

    @Constraints.Required
    public String Company;


    @Constraints.Required
    public String Gender;

    @Constraints.Required
    public String Languages;

    @Constraints.Required
    public String Image;



    public static Model.Finder<Long, MExpectedVisitors> findVisitorsRecords = new Model.Finder<Long, MExpectedVisitors>(Long.class, MExpectedVisitors.class);
    public static List<MExpectedVisitors> findAll() { return findVisitorsRecords.where().findList();}





    public static MExpectedVisitors findVisitorById(String id){
        return MExpectedVisitors.findVisitorsRecords.where().eq("NationalId",id)
                .findUnique();
    }

    public static List<MExpectedVisitors> findRecurrentVistorList(String Id){
        return findVisitorsRecords.where().eq("NationalId",Id).findList();
    }
}
